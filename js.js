let fonDark = false;
const setting = JSON.parse(localStorage.getItem('settings'));
if(setting){
    fonDark = setting.fonDark;
} 

let ul = document.querySelector('.tabs');
let li= document.querySelectorAll('.tabs-title');
let activeClass = document.querySelector('.active');
let text = document.querySelector('.tabs-content');

let buttonStyle = document.createElement('button');

buttonStyle.style.background = '#ccccff';
buttonStyle.style.borderColor = '#ccccff';
buttonStyle.innerText = 'Змінити тему';
document.body.append(buttonStyle);

if (fonDark){
    document.body.style.background = '#ccccff';
    buttonStyle.style.background = 'white';
    buttonStyle.style.borderColor = 'white';  
    li.forEach((el)=>{
        el.classList.add('perple');
    })
}; 

buttonStyle.addEventListener('click',()=>{
    fonDark = !fonDark;
    localStorage.setItem('settings',JSON.stringify({fonDark}));
    if (fonDark){
        buttonStyle.style.background = 'white';
        buttonStyle.style.borderColor = 'white'; 
        document.body.style.background = '#ccccff';
        li.forEach((el)=>{
            el.classList.add('perple');
        })
    } else{ 
        buttonStyle.style.background = '#ccccff';
        buttonStyle.style.borderColor = 'white';
        document.body.style.background ='white';
        li.forEach((el)=>{
            el.classList.remove('perple');
        })
    }; 
});

invisibleTextCreatAtribute();
visible(activeClass);
ul.addEventListener('click', (event) => {
    visible(event.target);
    if (activeClass) {
            activeClass.classList.remove('active');
        }
    event.target.classList.add('active');
    activeClass = event.target;
});
function invisibleTextCreatAtribute() {
    let i = 0;
    let li = ul.querySelectorAll('li');
    for (let p of text.querySelectorAll('li')) {
        let atrbute = document.createAttribute('data-name');
        if (li[i] === undefined) {
            console.log("There is a text that is not available to the user and must be added to the menu");
        } else {
            atrbute.value = li[i].innerText;
            p.setAttributeNode(atrbute);
            i++;
        };
    };
};
function visible(target) {
    for (let p of text.querySelectorAll('li')) {
        p.hidden = p.dataset.name !== target.innerText;
        
    }
};